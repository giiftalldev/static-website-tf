variable "bucket_name" {
  type    = "string"
  default = ""
}

variable "cors_domains" {
  type    = "list"
  default = ["*.giift.com"]
}
