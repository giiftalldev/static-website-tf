# provider "aws" {}

resource "aws_s3_bucket" "bucket" {
  bucket_prefix = "${var.bucket_name}"
  acl           = "private"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.bucket-logs.bucket}"
    target_prefix = "s3/"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["${var.cors_domains}"]
  }
}

resource "aws_s3_bucket_policy" "policy" {
  bucket = "${aws_s3_bucket.bucket.bucket}"

  policy = <<POLICY
{
"Version": "2012-10-17",
"Id": "S3StaticDocsAccessPolicy",
"Statement": [
    {
      "Sid": "Allow CloudFront Access",
      "Effect": "Allow",
      "Action": "s3:GetObject",
      "Principal": {"AWS": "${aws_cloudfront_origin_access_identity.cf_oai.iam_arn}"},
      "Resource": "${aws_s3_bucket.bucket.arn}/*"
    },
    {
      "Sid": "ForceSSLOnlyAccess",
      "Effect": "Deny",
      "Principal": { "AWS": "*" },
      "Action": "s3:*",
      "Resource": "${aws_s3_bucket.bucket.arn}/*",
      "Condition": { "Bool": { "aws:SecureTransport": "false" } }
    }
]
}
POLICY
}

resource "aws_s3_bucket" "bucket-logs" {
  bucket_prefix = "${var.bucket_name}-log"
  acl           = "log-delivery-write"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "logs-policy" {
  bucket = "${aws_s3_bucket.bucket-logs.bucket}"

  policy = <<POLICY
{
"Version": "2012-10-17",
"Id": "S3SecureTransportPolicy",
"Statement": [
    {
      "Sid": "ForceSSLOnlyAccess",
      "Effect": "Deny",
      "Principal": { "AWS": "*" },
      "Action": "s3:*",
      "Resource": "${aws_s3_bucket.bucket-logs.arn}/*",
      "Condition": { "Bool": { "aws:SecureTransport": "false" } }
    }
]
}
POLICY
}

resource "aws_cloudfront_origin_access_identity" "cf_oai" {
  comment = "To access private docs bucket"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket_regional_domain_name}"
    origin_id   = "${aws_s3_bucket.bucket.bucket}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.cf_oai.cloudfront_access_identity_path}"
    }
  }

  # #   aliases = ["${lookup(var.bucket_and_domain_names, terraform.workspace)}"]

  default_cache_behavior = {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["HEAD", "GET"]
    compress        = true
    default_ttl     = 86400

    forwarded_values = {
      query_string = true

      cookies = {
        forward = "none"
      }
    }

    target_origin_id       = "${aws_s3_bucket.bucket.bucket}"
    viewer_protocol_policy = "redirect-to-https"
  }
  enabled = true

  # #   default_root_object = "index.html"

  viewer_certificate = {
    minimum_protocol_version       = "TLSv1"
    cloudfront_default_certificate = true
    ssl_support_method             = "sni-only"
  }
  restrictions = {
    geo_restriction = {
      restriction_type = "none"
      locations        = []
    }
  }
  logging_config {
    include_cookies = false
    bucket          = "${aws_s3_bucket.bucket-logs.bucket_domain_name}"
    prefix          = "cloudfront/"
  }
}
